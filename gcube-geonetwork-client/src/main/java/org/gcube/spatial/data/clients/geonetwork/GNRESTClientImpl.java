package org.gcube.spatial.data.clients.geonetwork;

import org.gcube.spatial.data.clients.AbstractGenericRESTClient;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;

public class GNRESTClientImpl extends AbstractGenericRESTClient implements GNRESTInterface {

	private static final String API_BASE_PATH="rest";
	
	public GNRESTClientImpl(ConnectionDescriptor conn) {
		super(conn);
		setBasePath(API_BASE_PATH);		
	}
}
