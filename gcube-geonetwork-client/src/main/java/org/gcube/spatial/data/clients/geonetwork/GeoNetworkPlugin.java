package org.gcube.spatial.data.clients.geonetwork;

import org.gcube.spatial.data.clients.AbstractGenericClient;
import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.clients.model.engine.Range;

public class GeoNetworkPlugin extends AbstractGenericClient implements SDIGenericPlugin{
	
	public GeoNetworkPlugin() {
		super(GNRESTClientImpl.class);
	}
	
	
	@Override
	public ClientInfo getInfo() {
		return new ClientInfo("gn-rest", "GeoNetwork REST", "GeoNetwork REST client", "come class", 
				new Engine(Engine.GN_ENGINE, "Default GeoNetworkVersion", new Range("2.0.0", null)));
	}
}
