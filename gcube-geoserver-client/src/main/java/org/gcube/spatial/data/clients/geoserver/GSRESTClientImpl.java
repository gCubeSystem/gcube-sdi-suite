package org.gcube.spatial.data.clients.geoserver;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.gcube.spatial.data.clients.AbstractGenericRESTClient;
import org.gcube.spatial.data.clients.geoserver.model.FeatureTypeInfo;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.json.simple.JSONObject;
import org.opengis.feature.type.FeatureType;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GSRESTClientImpl extends AbstractGenericRESTClient implements GSRESTClient{

	private static final String API_BASE_PATH="rest";
	
	private static final String WS_BASE_PATH="workspaces";
	private static final String DATASTORE_BASE_PATH="datastores";
	private static final String STYLES_BASE_PATH="styles";
	private static final String LAYERS_BASE_PATH="layers";
	private static final String FEATURES_BASE_PATH="featuretypes";
	
	
	static Configuration JSON_PATH_ALWAYS_LIST_CONFIG=
			Configuration.builder().options(Option.ALWAYS_RETURN_LIST,Option.SUPPRESS_EXCEPTIONS,Option.DEFAULT_PATH_LEAF_TO_NULL).build();

	
	
	
	
	public GSRESTClientImpl(ConnectionDescriptor conn) {
		super(conn);

		setBasePath(API_BASE_PATH);
		
	}

	//************************** READ OPS
	
	
	@Override
	public JSONObject getWorkspaces() throws RemoteException,Exception {
		return get(WS_BASE_PATH,JSONObject.class);
	}

	@Override
	public JSONObject getWorkspace(String workspace) throws RemoteException, Exception {
		return get(WS_BASE_PATH+"/"+workspace,JSONObject.class);
	}
	
	@Override
	public JSONObject getDataStoresInWorkspace(String ws) throws RemoteException, Exception {
		return get(WS_BASE_PATH+"/"+ws+"/"+DATASTORE_BASE_PATH,JSONObject.class);
	}

	@Override
	public JSONObject getStyles() throws RemoteException, Exception {
		return get(STYLES_BASE_PATH,JSONObject.class);
	}
	
	@Override
	public JSONObject getStylesByLayer(String layer) throws RemoteException, Exception {
		return get(LAYERS_BASE_PATH+"/"+layer+"/"+STYLES_BASE_PATH,JSONObject.class);
	}
	
	
	@Override
	public JSONObject getStylesInWorkspace(String ws) throws RemoteException, Exception {
		return get(WS_BASE_PATH+"/"+ws+"/"+STYLES_BASE_PATH,JSONObject.class);
	}

	@Override
	public List<String> getWorkspaceNames() throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(getWorkspaces().toString());
		return sourceCtx.read("$..name");
	}

	@Override
	public List<String> getDataStoresNamesInWorkspace(String ws) throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(getDataStoresInWorkspace(ws).toString());
		return sourceCtx.read("$..name");
	}

	@Override
	public List<String> getStylesNames() throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(getStyles().toString());
		return sourceCtx.read("$..name");
	}


	@Override
	public List<String> getStylesNamesinWorkspace(String ws) throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(getStylesInWorkspace(ws).toString());
		return sourceCtx.read("$..name");
	}


	@Override
	public List<String> getStylesNamesByLayer(String layer) throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(getStylesByLayer(layer).toString());
		return sourceCtx.read("$..name");
	}

	
	@Override
	public JSONObject getDataStore(String workspace, String dsName) throws RemoteException, Exception {
		return get(WS_BASE_PATH+"/"+workspace+"/"+DATASTORE_BASE_PATH+"/"+dsName,JSONObject.class);
	}


	@Override
	public String getSLD(String styleName) throws RemoteException, Exception {
		return get(STYLES_BASE_PATH+"/"+styleName,String.class,"application/vnd.ogc.sld+xml");
	}
	
		
	
	@Override
	public FeatureTypeInfo getFeatureType(String workspace, String featureName) throws RemoteException, Exception {
		return get(WS_BASE_PATH+"/"+workspace+"/"+FEATURES_BASE_PATH+"/"+featureName,FeatureTypeInfo.class);
	}
	
	@Override
	public List<String> getFeatureTypesInDataStore(String workspace, String datastore)
			throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(
				get(WS_BASE_PATH+"/"+workspace+"/"+DATASTORE_BASE_PATH+"/"+datastore+"/"+FEATURES_BASE_PATH));
		return sourceCtx.read("$..name");
	}
	
	
	@Override
	public List<String> getFeatureTypesInWorkspace(String workspace) throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(
				get(WS_BASE_PATH+"/"+workspace+"/"+FEATURES_BASE_PATH));
		return sourceCtx.read("$..name");
	}
	
	
	@Override
	public JSONObject getLayer(String layerName) throws RemoteException, Exception {
		return get(LAYERS_BASE_PATH+"/"+layerName,JSONObject.class);
	}
	
	
	@Override
	public JSONObject getLayerInWorkspace(String ws, String layerName) throws RemoteException, Exception {
		return get(WS_BASE_PATH+"/"+ws+"/"+LAYERS_BASE_PATH+"/"+layerName,JSONObject.class);
	}
	
	
	@Override
	public List<String> getLayers() throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(
				get(LAYERS_BASE_PATH).toString());
		return sourceCtx.read("$..name");
	}
	
	
	@Override
	public List<String> getLayers(String workspace) throws RemoteException, Exception {
		DocumentContext sourceCtx=JsonPath.using(JSON_PATH_ALWAYS_LIST_CONFIG).parse(
				get(WS_BASE_PATH+"/"+workspace+"/"+LAYERS_BASE_PATH));
		return sourceCtx.read("$..name");
	}
	
	//************* DELETE OPS 
	
	
	@Override
	public void deleteWorkspace(String ws,boolean recurse) throws RemoteException, Exception {
		check(resolve().path(WS_BASE_PATH+"/"+ws).queryParams(Collections.singletonMap("recurse", new Object[] {recurse})).delete(),null);
	}

	
	
	@Override
	public void deleteStyle(String style,boolean purgeSLD,boolean updateReferences) throws RemoteException, Exception {
		Map<String,Object[]> params=new HashMap<>();
		params.put("recurse", new Object[] {updateReferences});
		params.put("purge", new Object[] {purgeSLD});
		check(resolve().path(STYLES_BASE_PATH+"/"+style).queryParams(params).delete(),null);
	}

		
	@Override
	public void deleteDataStore(String ds,boolean recurse) throws RemoteException, Exception {
		check(resolve().path(DATASTORE_BASE_PATH+"/"+ds).queryParams(Collections.singletonMap("recurse", new Object[] {recurse})).delete(),null);
	}

	
	
	@Override
	public void deleteLayer(String layer,boolean recurse) throws RemoteException, Exception {
		check(resolve().path(DATASTORE_BASE_PATH+"/"+layer).queryParams(Collections.singletonMap("recurse", new Object[] {recurse})).delete(),null);
	}


	//************************* CREATE OPS

	@Override
	public void createWorkspace(String ws) throws RemoteException, Exception {
		JSONObject obj=new JSONObject();
		obj.put("name", ws);
		post(WS_BASE_PATH,new JSONObject(Collections.singletonMap("workspace", obj)));
	}

	
	@Override
	public void publishDataStore(String ws, JSONObject parameters) throws Exception {
		post(WS_BASE_PATH+"/"+ws+"/"+DATASTORE_BASE_PATH, Entity.entity(parameters, MediaType.APPLICATION_JSON_TYPE));
	}
	
	@Override
	public void createStyle(String name, String content) throws RemoteException, Exception {
		post(STYLES_BASE_PATH, Entity.entity(content, "application/vnd.ogc.sld+xml"),String.class,Collections.singletonMap("name", name));
	}
	
	
	@Override
	public void createLayerAsFeatureType(String ws, String dataStoreName, FeatureTypeInfo ft)
			throws RemoteException, Exception {
		post(WS_BASE_PATH+"/"+ws+"/"+DATASTORE_BASE_PATH+"/"+dataStoreName+"/"+FEATURES_BASE_PATH, Entity.entity(
				new JSONObject(Collections.singletonMap("featureType",ft)), MediaType.APPLICATION_JSON_TYPE));
	}
	
}
