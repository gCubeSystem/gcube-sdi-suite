package org.gcube.spatial.data.clients.geoserver.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.json.simple.JSONObject;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@XmlRootElement(name = "datastore")
public class DataStoreRegistrationRequest {

	@Data
	@AllArgsConstructor
	private static class Parameter{
		private String key;
		 @XmlElement(name="$")
		private String value;
	}
	
	private JSONObject datastore=new JSONObject();
	
	private ArrayList<Parameter> paramList=new ArrayList<Parameter>();
	
	
	public DataStoreRegistrationRequest() {		
	}
	
	public DataStoreRegistrationRequest(String name) {
		setName(name);
	}
	
	/**
	 * See @method setParameters
	 * 
	 * @param name
	 * @param params
	 */
	public DataStoreRegistrationRequest(String name,Map<String,String> params) {
		setName(name);
		setParameters(params);
	}
	
	public DataStoreRegistrationRequest setName(String name) {
		datastore.put("name",name);
		return this;
	}
	
	/**
	 * Check parameters at
	 * https://docs.geoserver.org/latest/en/api/#1.0.0/datastores.yaml
	 * 
	 * @param params
	 * @return
	 */
	public DataStoreRegistrationRequest setParameters(Map<String,String> params) {
		
		params.forEach((String k,String v)->{paramList.add(new Parameter(k,v));});
		
		datastore.put("connectionParameters", new JSONObject(Collections.singletonMap("entry", paramList)));
		
		return this;
	}
	
	/**
	 * Check parameters at
	 * https://docs.geoserver.org/latest/en/api/#1.0.0/datastores.yaml
	 * 
	 * @param params
	 * @return
	 */
	public DataStoreRegistrationRequest param(String key,String value) {
		
		paramList.add(new Parameter(key,value));
		
		datastore.put("connectionParameters", new JSONObject(Collections.singletonMap("entry", paramList)));
		
		return this;
	}
}
