package org.gcube.spatial.data.clients.geoserver.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@JsonInclude(Include.NON_NULL)
public class FeatureTypeInfo {
	
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class Namespace{
		private String name;
		private String href;
	}
	
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class Keyword{
		private List<String> string;
	}
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class MetadataLink{
		private String type;
		private String metadataType;
		private String content;
	}
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class BoundingBox{
		private Number minx;
		private Number maxx;
		private Number miny;
		private Number maxy;
		private String crs;
	}
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class Entry{
		private String key;
		private String value;
	}
	
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class Store{
		private String clazz;
		private String name;
		private String href;
	}
	

	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class AttributeList{
		public List<Attribute> attribute;
	}
	
	
	@Data
	@JsonInclude(Include.NON_NULL)
	@AllArgsConstructor
	public static class Attribute{
		
		private String name;
		private Integer minOccurs;
		private Integer maxOccurs;
		private Boolean nillable;
		private String binding;
		private Integer length;
	}
	
	private String name;
	private String nativeName;
	private Namespace namespace;
	private String title;
	
	
	@XmlElement(name="abstract")
	private String abstractField;
	private List<Keyword> keywords;
 	
	private List<MetadataLink> metadataLinks;
	private List<MetadataLink> dataLinks;
	
	
	private String nativeCRS;
	private String srs;
	private BoundingBox nativeVoundingBox;
	private BoundingBox latLonBoundingBox;
	
	
	private List<Entry> metadata;
	
	private Store store;
	
	private String cqlFilter;
	private Integer maxFeatures;
	private Number numDecimals;
	private String responseSRS;
	private Boolean overridingServiceSRS;
	private Boolean skipNumberMatched;
	private Boolean circularArcPresent;
	private Number linearizationTolerance;
	
	private AttributeList attributes;
	
}
