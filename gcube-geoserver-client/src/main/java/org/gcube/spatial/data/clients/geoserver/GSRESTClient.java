package org.gcube.spatial.data.clients.geoserver;

import java.util.List;

import org.gcube.spatial.data.clients.GenericLoginClient;
import org.gcube.spatial.data.clients.geoserver.model.FeatureTypeInfo;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.json.simple.JSONObject;

/**
 * 
 * Based on https://docs.geoserver.org/latest/en/user/rest/index.html
 * 
 * @author FabioISTI
 *
 */
public interface GSRESTClient extends GenericLoginClient{
	
	// ********** READ OPS 
	
	// WS
	
	public JSONObject getWorkspaces()throws RemoteException,Exception;
	public List<String> getWorkspaceNames()throws RemoteException,Exception;
	public JSONObject getWorkspace(String workspace)throws RemoteException, Exception;
	
	// DS
	public JSONObject getDataStoresInWorkspace(String ws)throws RemoteException,Exception;
	public List<String> getDataStoresNamesInWorkspace(String ws)throws RemoteException,Exception;
	public JSONObject getDataStore(String workspace,String dsName)throws RemoteException,Exception;
	
	
	
	// SLD
	public JSONObject getStyles()throws RemoteException,Exception;
	public List<String> getStylesNames()throws RemoteException,Exception;
	public JSONObject getStylesInWorkspace(String ws)throws RemoteException,Exception;
	public List<String> getStylesNamesinWorkspace(String ws)throws RemoteException,Exception;
	public JSONObject getStylesByLayer(String layer)throws RemoteException,Exception;
	public List<String> getStylesNamesByLayer(String layer)throws RemoteException,Exception;
	public String getSLD(String styleName)throws RemoteException,Exception;
	
	
	// FeatureTypes
	public List<String> getFeatureTypesInWorkspace(String workspace)throws RemoteException,Exception;
	public List<String> getFeatureTypesInDataStore(String workspace,String datastore)throws RemoteException,Exception;
	public FeatureTypeInfo getFeatureType(String workspace,String featureName)throws RemoteException,Exception;
	
	
	//Layers
	public List<String> getLayers()throws RemoteException,Exception;
	public List<String> getLayers(String workspace)throws RemoteException, Exception;
	public JSONObject getLayer(String layerName)throws RemoteException, Exception;
	public JSONObject getLayerInWorkspace(String ws,String layerName)throws RemoteException, Exception;
	
	// *********** CREATE OPS
	public void createWorkspace(String ws)throws RemoteException,Exception;
	
	public void createStyle(String name,String content)throws RemoteException,Exception;
	
	/**
	 * Publish a DS described in @param parameters. Parameters vary depending on DS type. See https://docs.geoserver.org/latest/en/api/#1.0.0/datastores.yaml for more details.
	 * 
	 * @param ws
	 * @param parameters
	 * @return
	 */
	public void publishDataStore(String ws, JSONObject parameters)throws RemoteException,Exception;
	
	
	
	/**
	 * 
	 * /workspaces/{workspaceName}/coveragestores/{coveragestoreName}/coverages, 
	 * /workspaces/{workspaceName}/datastores/{datastoreName}/featuretypes, 
	 * /workspaces/{workspaceName}/wmsstores/{wmsstoreName}/wmslayers, or 
	 * /workspaces/{workspaceName}/wmtsstores/{wmststoreName}/wmtslayers
	 * 
	 * @param ws
	 * @param recurse
	 * @throws RemoteException
	 * @throws Exception
	 */
	
	
	public void createLayerAsFeatureType(String ws,String dataStoreName,FeatureTypeInfo ft)throws RemoteException, Exception;
	
	
//	// +********** DELETE OPS 
	
	
	public void deleteWorkspace(String ws, boolean recurse)throws RemoteException,Exception;
	public void deleteStyle(String style, boolean purgeSLDFile,boolean updateReferences)throws RemoteException,Exception;
	
	
	public void deleteDataStore(String ds, boolean recurse)throws RemoteException,Exception;
	public void deleteLayer(String layer,boolean recurse)throws RemoteException,Exception;
}
