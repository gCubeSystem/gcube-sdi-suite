package org.gcube.spatial.data.clients.geoserver;

import org.gcube.spatial.data.clients.AbstractGenericClient;
import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.clients.model.engine.Range;

public class GeoServerPlugin extends AbstractGenericClient implements SDIGenericPlugin{

	
	public GeoServerPlugin() {
		super(GSRESTClientImpl.class);
	}
	
	@Override
	public ClientInfo getInfo() {
		return new ClientInfo("gs-rest", "GeoServer REST", "GeoServer REST client", GSRESTClientImpl.class.getCanonicalName(), 
				new Engine(Engine.GS_ENGINE, "Default GeoServerVersion", new Range("2.0.0", null)));
	}
	
	
	
	
}
