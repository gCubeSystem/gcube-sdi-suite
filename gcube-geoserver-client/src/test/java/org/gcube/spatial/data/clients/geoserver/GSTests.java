package org.gcube.spatial.data.clients.geoserver;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.gcube.sdi.test.GCubeSDITest;
import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.geoserver.model.FeatureTypeInfo;
import org.gcube.spatial.data.clients.geoserver.model.FeatureTypeInfo.AttributeList;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.gcube.spatial.data.sdi.utils.Files;
import org.junit.Test;

public class GSTests extends GCubeSDITest{


	protected GSRESTClient getClient() throws RemoteException, Exception {
		SDIGenericPlugin obj=manager().getClientByEngineId(Engine.GS_ENGINE);
		assertTrue(obj.getClass().isAssignableFrom(GeoServerPlugin.class));
		return (GSRESTClient) obj.getRESTClient();
	}

	@Test
	public void basics() throws RemoteException, Exception {
		assumeTrue(isTestInfrastructureEnabled());
		GSRESTClient client=getClient();
		assertTrue(client.exist());	
		client.authenticate();
	}


	@Test
	public void gets() throws RemoteException, Exception {
		assumeTrue(isTestInfrastructureEnabled());


		GSRESTClient client=getClient();

		client.authenticate();
		client.getWorkspaceNames().forEach((String ws)->{
			try {
				if(ws!=null) {
					System.out.println("************************************ WS ***************************");
					System.out.println(client.getWorkspace(ws));
					for(String sld:client.getStylesNamesinWorkspace(ws))
						if(sld!=null)try { 
							System.out.println(client.getSLD(sld));
						}catch(RemoteException e ) {System.err.println("Unable to read SLD "+sld+" from "+ws+". Cause "+e.getResponseHTTPCode()+": "+e.getMessage());}


					System.out.println("WS "+ws+" ->"+client.getDataStoresInWorkspace(ws));

//					for(String ds:client.getDataStoresNamesInWorkspace(ws))
//						if(ds!=null) {
//							try{ System.out.println(client.getDataStore(ws, ds));							
//							}catch(RemoteException e ) {System.err.println("Unable to read DS "+ds+" from "+ws+". Cause "+e.getResponseHTTPCode()+": "+e.getMessage());}
//						}
					
					
					for(String ft : client.getFeatureTypesInWorkspace(ws))
						if(ft!=null)try {
							assertTrue(client.getFeatureType(ws, ft).getNativeName()!=null);
							System.out.println(client.getFeatureType(ws, ft));
						}catch(RemoteException e ) {System.err.println("Unable to read FT "+ft+" from "+ws+". Cause "+e.getResponseHTTPCode()+": "+e.getMessage());}
					
					
					for(String l : client.getLayers(ws))
						if(l!=null)try {
							System.out.println(client.getLayerInWorkspace(ws, l));
							System.out.println(client.getLayer(l));
						}catch(RemoteException e ) {System.err.println("Unable to read LA "+l+" from "+ws+". Cause "+e.getResponseHTTPCode()+": "+e.getMessage());}
					
				}
			}catch(Exception e) {
				System.err.println("Unable to check "+ws+" : "+e);
			}
			
			
				
		});
		
		System.out.println("Listing layers without ws");
//		client.getLayers().forEach((String l)->{
//			try{System.out.println(client.getLayer(l));
//			}catch(RemoteException e ) {System.err.println("Unable to read LA "+l+". Cause "+e.getResponseHTTPCode()+": "+e.getMessage());}
//			catch(Exception e ) {System.err.println("Unable to read LA "+l+". Cause : "+e.getMessage());}
//		});
	}


		@Test 
		public void CRUDS() throws RemoteException, Exception {
			assumeTrue(isTestInfrastructureEnabled());
			GSRESTClient client=getClient();
			
			client.authenticate();
			String ws=UUID.randomUUID().toString().replace("-", "_");
			client.createWorkspace(ws);
			client.getWorkspace(ws);
			
			
			
			// SLD
			String myStyle=UUID.randomUUID().toString().replace("-", "_");
			System.out.println("style name is :"+ myStyle);
			client.createStyle(myStyle,read("clustered_points.sld"));
			assertTrue("SLD registered ",client.getStylesNames().contains(myStyle));
			client.deleteStyle(myStyle, true, true);
			assertFalse("SLD Removed",client.getStylesNames().contains(myStyle));
			
			// DS
//			String myDS=UUID.randomUUID().toString().replace("-", "_");
//			
//			HashMap<String,String> parameters=new HashMap<String, String>();
//			
////			parameters.put("dbtype","gpkg");
//			parameters.put("url","gpkg");
//			
//			client.publishDataStore(ws, 
//					new DataStoreRegistrationRequest(myDS,parameters).getDatastore());
//			
//			client.deleteWorkspace(ws,true);
//
//			
			//FT
			
			
			FeatureTypeInfo ft= new FeatureTypeInfo();
			ft.setName(UUID.randomUUID().toString().replace("-", "_"));		
			ft.setNativeCRS("EPSG:4326");
			
			List<FeatureTypeInfo.Attribute> atts=new ArrayList<FeatureTypeInfo.Attribute>();
			atts.add(new FeatureTypeInfo.Attribute("the_geom",1,1,true,"com.vividsolutions.jts.geom.Point",0));
			
			ft.setAttributes(new AttributeList(atts));
			System.out.println("Writing "+ft);
			client.createLayerAsFeatureType("aquamaps", "timeseriesws", ft);
			
		}
		
		
		private static final String read(String toRead) throws IOException {
			File f= Files.getFileFromResources(toRead);
			return Files.readFileAsString(f.getAbsolutePath(), Charset.defaultCharset());
		}
}
