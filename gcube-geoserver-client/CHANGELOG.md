This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.gcube-geoserver-client

## [v1.0.0-SNAPSHOT] - 2021-2-11
First release
