# Changelog for org.gcube.spatial.data.sdi-library

## [v1.3.0-SNAPSHOT] 2020-07-21
- Offer GIS basic functionalities


## [v1.2.0] 2020-07-21

## Enhancements

- Application Profile (https://support.d4science.org/issues/18939)


### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)