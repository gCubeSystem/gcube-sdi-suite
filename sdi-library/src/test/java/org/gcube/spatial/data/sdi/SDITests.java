package org.gcube.spatial.data.sdi;

import static org.junit.Assume.assumeTrue;

import java.net.MalformedURLException;

import org.gcube.sdi.test.GCubeSDITest;
import org.gcube.spatial.data.clients.geoserver.GSRESTClient;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.sdi.interfaces.SDIManagement;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.gcube.spatial.data.sdi.plugins.SDIAbstractPlugin;
import org.junit.Test;

public class SDITests extends GCubeSDITest{

	@Test
	public void getScopeConfiguration() throws RemoteException, IllegalArgumentException, MalformedURLException {
		assumeTrue(isTestInfrastructureEnabled());
		SDIManagement sdi=SDIAbstractPlugin.management().build();
		System.out.println(sdi.getConfiguration());
	}
	
	
	@Test
	public void getScopeHealth() throws RemoteException {
		assumeTrue(isTestInfrastructureEnabled());
		SDIManagement sdi=SDIAbstractPlugin.management().build();
		System.out.println(sdi.getReport());
	}
	
	
	@Test
	public void getGS() throws Exception {
		assumeTrue(isTestInfrastructureEnabled());
		System.out.println(
				SDIAbstractPlugin.management().build().getClientByEngineId(Engine.GS_ENGINE).getInfo());
		
//		GSRESTClient gs=(GSRESTClient) SDIAbstractPlugin.management().build().getClientByEngineId(Engine.GS_ENGINE).getRESTClient();
//		gs.getWorkspaces();
//		
	}
}
