package org.gcube.spatial.data.sdi;

import static org.junit.Assert.fail;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import org.gcube.sdi.test.GCubeSDITest;
import org.gcube.spatial.data.sdi.interfaces.Metadata;
import org.gcube.spatial.data.sdi.model.metadata.MetadataPublishOptions;
import org.gcube.spatial.data.sdi.model.metadata.TemplateInvocationBuilder;
import org.gcube.spatial.data.sdi.plugins.SDIAbstractPlugin;
import org.junit.Test;

public class MetadataTests extends GCubeSDITest{
	
	
	
	
	@Test
	public void getAvailableTemplatesTest() throws IllegalArgumentException, URISyntaxException{
		assumeTrue(isTestInfrastructureEnabled());
//		Metadata meta=SDIAbstractPlugin.metadata().at(new URI("http://"+sdiHostname+"/sdi-service/gcube/service")).build();
		Metadata meta=SDIAbstractPlugin.metadata().build();
		System.out.println(meta.getAvailableTemplates());
	}

	
	@Test
	public void pushMetadata() throws IllegalArgumentException, URISyntaxException{
		assumeTrue(isTestInfrastructureEnabled());
		File metadata=Paths.get("src/test/resources/toPublishMeta").toFile();
		
		Metadata meta=SDIAbstractPlugin.metadata().build();
		for(File f:metadata.listFiles(new FilenameFilter() {			
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xml");
			}
		})) {
			
			try {
//			System.out.println(meta.pushMetadata(f));
			MetadataPublishOptions opts=new MetadataPublishOptions(new TemplateInvocationBuilder().threddsOnlineResources("my_hostname", "some_dataset.nc", "myPersonalCatalog").get());
			opts.setGeonetworkCategory("service");
			opts.setValidate(false);
			System.out.println(meta.pushMetadata(f, opts));
			}catch(Exception e) {
				
				throw new RuntimeException("Unable to push "+f.getName(),e);
			}
		}
	}
	

//	@Test
//	public void validate() throws IllegalArgumentException, URISyntaxException{
//		assumeTrue(isTestInfrastructureEnabled());
//		File metadata=Paths.get("src/test/resources/toPublishMeta").toFile();
//		
//		Metadata meta=SDIAbstractPlugin.metadata().build();
//		for(File f:metadata.listFiles(new FilenameFilter() {			
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.endsWith(".xml");
//			}
//		})) {
//			
////			System.out.println(meta.pushMetadata(f));
//			MetadataPublishOptions opts=new MetadataPublishOptions(new TemplateInvocationBuilder().threddsOnlineResources("my_hostname", "some_dataset.nc", "myPersonalCatalog").get());
//			opts.setGeonetworkCategory("service");
//			opts.setValidate(false);
//			System.out.println(meta.pushMetadata(f, opts));
//		}
//	}
	
	
	
}
