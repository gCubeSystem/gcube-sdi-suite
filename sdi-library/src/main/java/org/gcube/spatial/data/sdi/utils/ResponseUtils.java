package org.gcube.spatial.data.sdi.utils;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResponseUtils {

	public static <T> T check(GXInboundResponse resp, Class<T> clazz) throws RemoteException{
		try {
		log.debug("Checking Response {}",resp);
		
		if(resp.hasGXError()||!resp.getSource().getStatusInfo().getFamily().equals(Family.SUCCESSFUL)) {			
			RemoteException e=new RemoteException("Error received from server");
			e.setRemoteMessage(resp.getMessage());
			e.setResponseHTTPCode(resp.getHTTPCode());
			e.setContent(resp.getStreamedContentAsString());
			throw e;
		}else {
			if(clazz==null) return null;			
			
			if(clazz==String.class) return (T) resp.getStreamedContentAsString();
			
			return resp.tryConvertStreamedContentFromJson(clazz);
		}
		}catch(RemoteException e) {
			throw e;
		}catch(Exception e) {
			throw new RemoteException("Unable to read  response from server.",e);
		}
	}
	
	
	public static Response check(GXInboundResponse resp) throws RemoteException{
		try {
		log.debug("Checking Response {}",resp);
		
		if(resp.hasGXError()||!resp.getSource().getStatusInfo().getFamily().equals(Family.SUCCESSFUL)) {			
			RemoteException e=new RemoteException("Error received from server");
			e.setRemoteMessage(resp.getMessage());
			e.setResponseHTTPCode(resp.getHTTPCode());
			e.setContent(resp.getStreamedContentAsString());
			throw e;
		}else {
			return resp.getSource();
		}
		}catch(RemoteException e) {
			throw e;
		}catch(Exception e) {
			throw new RemoteException("Unable to read  response from server.",e);
		}
	}
	
//	protected static <T> T check(GXInboundResponse resp, Class<T> clazz) throws RemoteException{
//		try {
//		log.debug("Checking Response {}",resp);
//		if(resp.hasGXError()) {			
//			RemoteException e=new RemoteException("Error received from server");
//			e.setRemoteMessage(resp.getMessage());
//			e.setResponseHTTPCode(resp.getHTTPCode());
//			e.setContent(resp.getStreamedContentAsString());
//			throw e;
//		}else {
//			if(clazz==null) return null;			
//			
//			if(clazz==String.class) return (T) resp.getStreamedContentAsString();
//			
//			return resp.tryConvertStreamedContentFromJson(clazz);
//		}
//		}catch(RemoteException e) {
//			throw e;
//		}catch(Exception e) {
//			throw new RemoteException("Unable to read  response from server.",e);
//		}
//	}
	
}
