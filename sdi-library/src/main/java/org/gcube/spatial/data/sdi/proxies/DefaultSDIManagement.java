package org.gcube.spatial.data.sdi.proxies;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.spatia.data.model.profiles.ApplicationProfile;
import org.gcube.spatial.data.clients.SDIClientManager;
import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.sdi.interfaces.SDIManagement;
import org.gcube.spatial.data.sdi.model.ScopeConfiguration;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.gcube.spatial.data.sdi.model.health.HealthReport;
import org.gcube.spatial.data.sdi.model.service.GeoServiceDescriptor;
import org.gcube.spatial.data.sdi.utils.ResponseUtils;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class DefaultSDIManagement implements SDIManagement{

	private final ProxyDelegate<GXWebTargetAdapterRequest> delegate;

	private SDIClientManager clientManager=null;

	public DefaultSDIManagement(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		this.delegate=delegate;
		clientManager=new SDIClientManager();
		log.debug("Available clients are ");
		clientManager.list().forEach((ClientInfo c)->{
			log.debug("{}",c);
		});
	}


	@Override
	public ScopeConfiguration getConfiguration() throws RemoteException {
		Call<GXWebTargetAdapterRequest, ScopeConfiguration> call=
				new Call<GXWebTargetAdapterRequest,ScopeConfiguration>(){
			@Override
			public ScopeConfiguration call(GXWebTargetAdapterRequest endpoint) throws Exception {
				return ResponseUtils.check(endpoint.get(),ScopeConfiguration.class);
			}
		};
		try {
			return delegate.make(call);			
		}catch(Exception e) {
			throw new RemoteException(e);
		}
	}

	@Override
	public HealthReport getReport() throws RemoteException {
		Call<GXWebTargetAdapterRequest, HealthReport> call=new Call<GXWebTargetAdapterRequest,HealthReport>(){
			@Override
			public HealthReport call(GXWebTargetAdapterRequest endpoint) throws Exception {
				return ResponseUtils.check(endpoint.setAcceptedResponseType(MediaType.APPLICATION_JSON_TYPE).path(ServiceConstants.SDI.STATUS_PATH).
						get(),HealthReport.class);
			}
		};
		try {
			return delegate.make(call);			
		}catch(Exception e) {
			throw new RemoteException(e);
		}
	}


	@Override
	public ApplicationProfile getProfile(String serviceClass,String serviceName) throws RemoteException {
		Call<GXWebTargetAdapterRequest, ApplicationProfile> call=new Call<GXWebTargetAdapterRequest,ApplicationProfile>(){
			@Override
			public ApplicationProfile call(GXWebTargetAdapterRequest endpoint) throws Exception {
				return ResponseUtils.check(endpoint.path(ServiceConstants.SDI.PROFILES_PATH).path(serviceClass).
						path(serviceName).get(),ApplicationProfile.class);
			}
		};
		try {
			return delegate.make(call);			
		}catch(Exception e) {
			throw new RemoteException(e);
		}
	}

	@Override
	public SDIGenericPlugin getClientByEngineId(String engineId) throws Exception {		
		List<? extends GeoServiceDescriptor> desc=getConfiguration().getByEngine(engineId);
				
		if(desc==null|| desc.isEmpty())	throw new Exception("No "+engineId+" available in current context. Check ScopeConfiguration object.");
		
		for(GeoServiceDescriptor d:desc) {
			SDIGenericPlugin c=clientManager.get(d);
			if(c!=null) return c;
		}
		
		throw new RuntimeException("No implementation available for "+engineId); 
	}
}
