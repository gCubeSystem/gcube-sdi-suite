package org.gcube.spatial.data.sdi.proxies;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.spatial.data.sdi.interfaces.Metadata;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.metadata.MetadataPublishOptions;
import org.gcube.spatial.data.sdi.model.metadata.MetadataReport;
import org.gcube.spatial.data.sdi.model.metadata.TemplateCollection;
import org.gcube.spatial.data.sdi.model.metadata.TemplateDescriptor;
import org.gcube.spatial.data.sdi.model.metadata.TemplateInvocation;
import org.gcube.spatial.data.sdi.utils.ResponseUtils;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

public class DefaultMetadata implements Metadata {

	private final ProxyDelegate<GXWebTargetAdapterRequest> delegate;


	public DefaultMetadata(ProxyDelegate<GXWebTargetAdapterRequest> config){		
		this.delegate = config;
	}

	
	
	@Override
	public TemplateCollection getAvailableTemplates() {
		
		Call<GXWebTargetAdapterRequest, Set<TemplateDescriptor>> call = new Call<GXWebTargetAdapterRequest, Set<TemplateDescriptor>>() {
			@Override
			public Set<TemplateDescriptor> call(GXWebTargetAdapterRequest templates) throws Exception {
				GenericType<Set<TemplateDescriptor>> generic=new GenericType<Set<TemplateDescriptor>>() {					
				};
				return ResponseUtils.check(templates.path(ServiceConstants.Metadata.LIST_METHOD).get()).readEntity(generic);
			}
		};
		try {
			return new TemplateCollection(new HashSet<TemplateDescriptor>(delegate.make(call)));			
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public MetadataReport pushMetadata(File toPublish) {
		return pushMetadata(toPublish, new MetadataPublishOptions());
	}

	@Override
	public MetadataReport pushMetadata(final File toPublish, final MetadataPublishOptions options) {
		// upload Meta

		Call<GXWebTargetAdapterRequest,MetadataReport> applyTemplatesCall=null; //needs uploaded id
		Call<GXWebTargetAdapterRequest,MetadataReport> publishCall=null; 		//needs uploaded id
		
		try{
			Call<GXWebTargetAdapterRequest,String> uploadCall=new Call<GXWebTargetAdapterRequest, String>() {

				@Override
				public String call(GXWebTargetAdapterRequest endpoint) throws Exception {
					endpoint.register(MultiPartFeature.class);
					FormDataMultiPart multi=new FormDataMultiPart();
//					multi.field("file",toPublish,MediaType.APPLICATION_OCTET_STREAM_TYPE);
					FileDataBodyPart fileDataBodyPart = new FileDataBodyPart(ServiceConstants.Metadata.UPLOADED_FILE_PARAMETER,
							toPublish,MediaType.APPLICATION_OCTET_STREAM_TYPE);
					multi.bodyPart(fileDataBodyPart);
					return ResponseUtils.check(endpoint.post(Entity.entity(multi, multi.getMediaType())),String.class);					
				}
			};


			final String id=delegate.make(uploadCall);

			applyTemplatesCall=new Call<GXWebTargetAdapterRequest, MetadataReport>() {
				@Override
				public MetadataReport call(GXWebTargetAdapterRequest endpoint) throws Exception {
					
					return ResponseUtils.check(endpoint.path(id).put(Entity.entity(								
							new HashSet<TemplateInvocation>(options.getTemplateInvocations()),MediaType.APPLICATION_JSON)), MetadataReport.class);
				}
			};
			
			publishCall=new Call<GXWebTargetAdapterRequest,MetadataReport>(){
				@Override
				public MetadataReport call(GXWebTargetAdapterRequest endpoint) throws Exception {
					
					Map<String,String> queryParams = new WeakHashMap<>();
					queryParams.put(ServiceConstants.Metadata.VALIDATE_PARAMETER, options.isValidate()+"");
					queryParams.put(ServiceConstants.Metadata.PUBLIC_PARAMETER, options.isMakePublic()+"");
					queryParams.put(ServiceConstants.Metadata.STYLESHEET_PARAMETER, options.getGeonetworkStyleSheet());
					
					return ResponseUtils.check(endpoint.path(ServiceConstants.Metadata.PUBLISH_METHOD).path(id).path(options.getGeonetworkCategory()).
							get(), MetadataReport.class);
					
				}
			};			
		}catch(Throwable t){
			throw new RuntimeException("Unable to upload file.",t);
		}


		//APPLY TEMPLATES
		MetadataReport templateReport =null;
		try{
			if(!options.getTemplateInvocations().isEmpty())
				templateReport=delegate.make(applyTemplatesCall);
		}catch(Throwable t){
			throw new RuntimeException("Unable to apply templates",t);
		}

		//PUBLISH

		
		try{
			MetadataReport publishReport=delegate.make(publishCall);
			if(templateReport!=null) publishReport.setAppliedTemplates(templateReport.getAppliedTemplates());
			return publishReport;
		}catch(Throwable t){
			throw new RuntimeException("Unable to publish metadata. ",t);
		}

	}

	
}
