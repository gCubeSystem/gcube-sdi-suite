package org.gcube.spatial.data.sdi.plugins;

import org.gcube.common.clients.Plugin;
import org.gcube.common.clients.ProxyBuilder;
import org.gcube.common.clients.ProxyBuilderImpl;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.spatial.data.sdi.interfaces.Metadata;
import org.gcube.spatial.data.sdi.interfaces.SDIManagement;
import org.gcube.spatial.data.sdi.model.ServiceConstants;

public abstract class SDIAbstractPlugin<S, P> implements Plugin<S, P>{

	
	private static final MetadataPlugin metadata_plugin=new MetadataPlugin();
	private static final SDIPlugin sdi_plugin=new SDIPlugin();
	
	
	
	public static ProxyBuilder<Metadata> metadata() {
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,Metadata>(metadata_plugin);
	}
	

	public static ProxyBuilder<SDIManagement> management() {
	    return new ProxyBuilderImpl<GXWebTargetAdapterRequest,SDIManagement>(sdi_plugin);
	}
	
	
	public final String name;

	public SDIAbstractPlugin(String name) {
		this.name = name;
	}
	
	@Override
	public String serviceClass() {
		return ServiceConstants.SERVICE_CLASS;
	}
	@Override
	public String serviceName() {
		return ServiceConstants.SERVICE_NAME;
	}
	@Override
	public String name() {
		return name;
	}
	@Override
	public String namespace() {
		return ServiceConstants.NAMESPACE;
	}
}
