package org.gcube.spatial.data.sdi.proxies;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.client.Entity;

import org.gcube.common.clients.Call;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AbstractProxy {

private ConnectionDescriptor conn;
	
	

	@Setter
	private String basePath=null;
	
	private Set<Class<?>> toRegisterClasses=new HashSet<Class<?>>();
	private Set<Object> toRegisterObjects=new HashSet<>();
	
	protected AbstractProxy(ConnectionDescriptor conn) {
		this.conn=conn;
	}
	
	protected void register(Class<?> providerClass) {
		toRegisterClasses.add(providerClass);
	}
	
	protected void register(Object provider) {
		toRegisterObjects.add(provider);
	}
	
	
	
	protected GXWebTargetAdapterRequest resolve() throws UnsupportedEncodingException {
		GXWebTargetAdapterRequest toReturn =GXWebTargetAdapterRequest.newHTTPSRequest(conn.getEndpoint());
		if(basePath!=null) return toReturn.path(basePath);
		
		else return toReturn;
	}
	
	
	protected <T> T makeCall(Call<GXWebTargetAdapterRequest, T> call) throws Exception{
		try{
			return call.call(resolve());
		}catch(RemoteException e) {
			throw e;
		}catch(Throwable t) {
			throw new Exception(t);
		}
	}
	
	
	protected void delete(String path) throws Exception {
		check(resolve().path(path).delete(),null);
	}
	
	protected void post(String path,Entity<?> entity)throws Exception{
		post(path,entity,null);
	}
	
	
	protected void post(String path,Object obj) {
		
	}
	
	protected <T> T post(String path,Entity<?> entity,Class<T> returnClazz) throws Exception {
		return makeCall(new Call<GXWebTargetAdapterRequest, T>() {
			
			public T call(GXWebTargetAdapterRequest endpoint) throws Exception {
				return  check(endpoint.path(path).post(entity),returnClazz);
			};
			
		});
	}
	
	protected String get(String path) throws Exception {
		return get(path,null);
	}
	
	protected <T> T get(String path, Class<T> clazz) throws Exception{
		return makeCall(new Call<GXWebTargetAdapterRequest, T>() {
			
			public T call(GXWebTargetAdapterRequest endpoint) throws Exception {
				return  check(endpoint.path(path).get(),clazz);
			};
			
		});
	}
	
	
	
	
	protected static <T> T check(GXInboundResponse resp, Class<T> clazz) throws RemoteException{
		try {
		log.debug("Checking Response {}",resp);
		if(resp.hasGXError()) {			
			RemoteException e=new RemoteException("Error received from server");
			e.setRemoteMessage(resp.getMessage());
			e.setResponseHTTPCode(resp.getHTTPCode());
			e.setContent(resp.getStreamedContentAsString());
			throw e;
		}else {
			if(clazz==null) return null;			
			
			if(clazz==String.class) return (T) resp.getStreamedContentAsString();
			
			return resp.tryConvertStreamedContentFromJson(clazz);
		}
		}catch(RemoteException e) {
			throw e;
		}catch(Exception e) {
			throw new RemoteException("Unable to read  response from server.",e);
		}
	}
}
