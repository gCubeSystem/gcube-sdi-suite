This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.sdi-service

## [v1.5.0-SNAPSHOT] 2020-05-15


## [v1.4.3-SNAPSHOT] 2020-05-15
changed maven repos

## [v1.4.2] 2020-05-15

### Fixes

- Integration with gcube distribution (https://support.d4science.org/issues/19612)
