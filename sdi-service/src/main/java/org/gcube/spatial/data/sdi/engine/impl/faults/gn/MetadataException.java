package org.gcube.spatial.data.sdi.engine.impl.faults.gn;

public class MetadataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -234185402179551404L;

	public MetadataException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MetadataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MetadataException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MetadataException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MetadataException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
