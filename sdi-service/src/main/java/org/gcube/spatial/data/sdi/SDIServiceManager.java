package org.gcube.spatial.data.sdi;

import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SDIServiceManager implements ApplicationManager {

	ApplicationContext ctx = ContextProvider.get();
	
	@Override
	public void onInit() {
		log.trace("************** INIT *****************");
		log.trace("ApplicationContext : {} ",ctx);
//		log.trace("ServletContext : {} ",ctx.application());
//		log.trace("ContainerContext : {} ",ctx.container());
//		log.trace("Lifecycle : {} ",ctx.lifecycle());
//		log.trace("ApplicationContext : {} ",ctx.name());
	}

	@Override
	public void onShutdown() {
		log.trace("************** Shutdown *****************");
		log.trace("ApplicationContext : {} ",ctx);
	}

}
