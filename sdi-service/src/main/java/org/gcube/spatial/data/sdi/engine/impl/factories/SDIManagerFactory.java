package org.gcube.spatial.data.sdi.engine.impl.factories;

import javax.inject.Inject;

import org.gcube.spatial.data.sdi.engine.GISManager;
import org.gcube.spatial.data.sdi.engine.GeoNetworkManager;
import org.gcube.spatial.data.sdi.engine.SDIManager;
import org.gcube.spatial.data.sdi.engine.ThreddsManager;
import org.gcube.spatial.data.sdi.engine.impl.SDIManagerImpl;
import org.glassfish.hk2.api.Factory;

import lombok.Synchronized;

public class SDIManagerFactory implements Factory<SDIManager>{

	@Override
	public void dispose(SDIManager instance) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public SDIManager provide() {
		return getInstance(gnManager,gisManager,thManager);
	}
	
	
	private static SDIManager sdiManager=null;
	
	@Inject
	private GeoNetworkManager gnManager;
	@Inject
	private GISManager gisManager;
	@Inject 
	private ThreddsManager thManager;
	
	
	
	@Synchronized 
	private static SDIManager getInstance(GeoNetworkManager gnManager, GISManager gisManager,ThreddsManager thManager) {
		if(sdiManager==null)
			sdiManager=new SDIManagerImpl(gnManager,thManager,gisManager);
		return sdiManager;
	}
}
