package org.gcube.spatial.data.sdi;

import javax.ws.rs.ApplicationPath;

import org.aopalliance.reflect.Metadata;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.spatial.data.sdi.engine.GISManager;
import org.gcube.spatial.data.sdi.engine.GeoNetworkManager;
import org.gcube.spatial.data.sdi.engine.RoleManager;
import org.gcube.spatial.data.sdi.engine.SDIManager;
import org.gcube.spatial.data.sdi.engine.TemplateManager;
import org.gcube.spatial.data.sdi.engine.TemporaryPersistence;
import org.gcube.spatial.data.sdi.engine.ThreddsManager;
import org.gcube.spatial.data.sdi.engine.impl.factories.GeoNetworkManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.GeoServerManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.MetadataTemplateManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.RoleManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.SDIManagerFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.TemporaryPersistenceFactory;
import org.gcube.spatial.data.sdi.engine.impl.factories.ThreddsManagerFactory;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.rest.GeoNetwork;
import org.gcube.spatial.data.sdi.rest.GeoServer;
import org.gcube.spatial.data.sdi.rest.SDI;
import org.gcube.spatial.data.sdi.rest.Thredds;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@ApplicationPath(ServiceConstants.APPLICATION)
public class SDIService extends ResourceConfig{

//	@Inject
//	MetadataTemplateManager templateManager;
//	@Inject
//	TemporaryPersistence persistence;
//	
	public SDIService() {
		super();
//		log.warn("Initializing App : Properties.. ");
//		ApplicationContext context=ContextProvider.get();		
////		ContainerConfiguration configuration=context.container().configuration();
//		try{
////			URL resourceUrl = context.application().getResource("/WEB-INF/config.properties");
////			LocalConfiguration.init(resourceUrl).
//			LocalConfiguration.setTemplateConfigurationObject(ContextProvider.get());
//			
//		}catch(Throwable t){
//			log.debug("Listing available paths");
//			for(Object obj:context.application().getResourcePaths("/WEB-INF"))
//				log.debug("OBJ : {} ",obj);
//			
//			throw new RuntimeException("Unable to load configuration properties",t);
//		}
		
		
		
		packages("org.gcube.spatial.data");
		
		
		log.info("Initializing App : Binders");
		
		AbstractBinder binder = new AbstractBinder() {
			@Override
			protected void configure() {                    
				bindFactory(SDIManagerFactory.class).to(SDIManager.class);
				bindFactory(GeoNetworkManagerFactory.class).to(GeoNetworkManager.class);
				bindFactory(ThreddsManagerFactory.class).to(ThreddsManager.class);
				bindFactory(GeoServerManagerFactory.class).to(GISManager.class);
				bindFactory(MetadataTemplateManagerFactory.class).to(TemplateManager.class);
				bindFactory(RoleManagerFactory.class).to(RoleManager.class);
				bindFactory(TemporaryPersistenceFactory.class).to(TemporaryPersistence.class);
				
			}
		};
		register(binder);
		
		

        register(MultiPartFeature.class);
        registerClasses(SDI.class);
        registerClasses(GeoNetwork.class);
        registerClasses(GeoServer.class);
        registerClasses(Thredds.class);
        registerClasses(Metadata.class);
        
        log.info("Initialization complete");        

	}
	
	
	
	
}
