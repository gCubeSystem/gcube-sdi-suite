package org.gcube.spatial.data.sdi.engine.impl.faults;

public class OutdatedServiceEndpointException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1874537989302709012L;

	public OutdatedServiceEndpointException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OutdatedServiceEndpointException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public OutdatedServiceEndpointException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public OutdatedServiceEndpointException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public OutdatedServiceEndpointException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
