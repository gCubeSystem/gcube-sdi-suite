package org.gcube.spatial.data.sdi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class FileUtils {

	public static File getFileFromResources(String fileName) {

        ClassLoader classLoader =LocalConfiguration.class.getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file "+fileName+" is not found!");
        } else {
            return new File(resource.getFile());
        }

    }
	
	public static InputStream open(String fileName) throws IOException {

        ClassLoader classLoader =LocalConfiguration.class.getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file "+fileName+" is not found!");
        } else {
            return resource.openStream();
        }

    }
	
}
