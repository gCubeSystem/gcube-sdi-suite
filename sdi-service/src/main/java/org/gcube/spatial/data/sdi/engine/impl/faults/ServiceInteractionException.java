package org.gcube.spatial.data.sdi.engine.impl.faults;

public class ServiceInteractionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4708440073435829969L;

	public ServiceInteractionException() {
		// TODO Auto-generated constructor stub
	}

	public ServiceInteractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServiceInteractionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ServiceInteractionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServiceInteractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
