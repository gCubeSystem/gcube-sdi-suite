package org.gcube.spatial.data.sdi.engine.impl.factories;

import org.gcube.spatial.data.sdi.engine.RoleManager;
import org.gcube.spatial.data.sdi.engine.impl.RoleManagerImpl;
import org.glassfish.hk2.api.Factory;

import lombok.Synchronized;

public class RoleManagerFactory implements Factory<RoleManager>{

	@Override
	public void dispose(RoleManager instance) {
	}
	
	
	private static RoleManager instance;
	
	@Override
	public RoleManager provide() {
		return getInstance();
	}
	
	@Synchronized
	private static RoleManager getInstance() {
		if(instance==null)
			instance=new RoleManagerImpl();
		return instance;
	}
}
