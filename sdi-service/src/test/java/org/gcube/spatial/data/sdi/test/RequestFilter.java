package org.gcube.spatial.data.sdi.test;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Provider
public class RequestFilter implements ContainerRequestFilter {
    
    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {
        String token=ctx.getHeaderString("gcube-token");
        log.debug("Catched token : "+token);
        TokenSetter.setToken(token);
    }
}
