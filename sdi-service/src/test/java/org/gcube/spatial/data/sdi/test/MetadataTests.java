package org.gcube.spatial.data.sdi.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Paths;
import java.util.Set;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.metadata.MetadataReport;
import org.gcube.spatial.data.sdi.model.metadata.TemplateInvocation;
import org.gcube.spatial.data.sdi.model.metadata.TemplateInvocationBuilder;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.junit.Test;

public class MetadataTests extends ServiceTest {

	
	
	@Test
	public void testUpload() {
		assumeTrue(isTestInfrastructureEnabled());
	}
	
	@Test
	public void testMetadataTemplates() throws Exception {
		assumeTrue(isTestInfrastructureEnabled());
		
	File metadata=Paths.get("src/test/resources/xml").toFile();
		
		
		for(File f:metadata.listFiles(new FilenameFilter() {			
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xml");
			}
		})) {
		
		publishMetadataFile(f, 
				new TemplateInvocationBuilder().
				threddsOnlineResources("my_hostname", "some_dataset.nc", "myPersonalCatalog").get());
		}
	}
	
	private void publishMetadataFile(File toPublish, Set<TemplateInvocation> templates) throws Exception {
		try {
		String id= pushMetadata(toPublish, target(ServiceConstants.Metadata.INTERFACE));
		
		MetadataReport applicationReport= applytemplate(id, target(ServiceConstants.Metadata.INTERFACE), templates);
		System.out.println(applicationReport);
		assertTrue(applicationReport.getAppliedTemplates().size()==templates.size());
		
		MetadataReport publishReport= publish(id, target(ServiceConstants.Metadata.INTERFACE), 
				true, true, ServiceConstants.Metadata.DEFAULT_CATEGORY, ServiceConstants.Metadata.DEFAULT_STYLESHEET);
		System.out.println(publishReport);
		assertTrue(publishReport.getPublishedUUID()!=null);
		}catch(Exception e) {
			throw new Exception("Failed test on xml file "+toPublish.getName());
		}
	}
	
	
	
	private static String pushMetadata(File toPublish,WebTarget endpoint) throws Exception {
		FormDataMultiPart multi=new FormDataMultiPart();
//		multi.field("file",toPublish,MediaType.APPLICATION_OCTET_STREAM_TYPE);
		FileDataBodyPart fileDataBodyPart = new FileDataBodyPart(ServiceConstants.Metadata.UPLOADED_FILE_PARAMETER,
				toPublish,MediaType.APPLICATION_OCTET_STREAM_TYPE);
		multi.bodyPart(fileDataBodyPart);
		Response resp= endpoint.request().post(Entity.entity(multi, multi.getMediaType()));
		checkResponse(resp);
		return resp.readEntity(String.class);
	}
	
	
	private static MetadataReport applytemplate(String uploadedID, WebTarget endpoint,Set<TemplateInvocation> templates) throws Exception {
		
		
		Response resp= endpoint.path(uploadedID).
				request(MediaType.APPLICATION_JSON).put(Entity.entity(templates,MediaType.APPLICATION_JSON));
		checkResponse(resp);
		return resp.readEntity(MetadataReport.class);
	}
	
	private static MetadataReport publish(String uploadedID,WebTarget endpoint,boolean validate, 
			boolean setPublic, String category, String stylesheet) throws Exception {
		Response resp= endpoint.path(ServiceConstants.Metadata.PUBLISH_METHOD).path(uploadedID).path(category).
				queryParam(ServiceConstants.Metadata.VALIDATE_PARAMETER, validate).
				queryParam(ServiceConstants.Metadata.PUBLIC_PARAMETER, setPublic).
				queryParam(ServiceConstants.Metadata.STYLESHEET_PARAMETER, stylesheet).
				request(MediaType.APPLICATION_JSON).get();
		checkResponse(resp);
		return resp.readEntity(MetadataReport.class);
	}
}
