package org.gcube.spatial.data.sdi.test;

import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.sdi.engine.impl.metadata.TemplateApplicationReport;
import org.gcube.spatial.data.sdi.model.ScopeConfiguration;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.metadata.TemplateDescriptor;
import org.gcube.spatial.data.sdi.model.metadata.TemplateInvocationBuilder;
import org.junit.Test;

public class MainTest extends ServiceTest{
	
	
	@Test
	public void getConfiguration(){
		assumeTrue(isTestInfrastructureEnabled());
		System.out.println(target(ServiceConstants.SDI.INTERFACE).request(MediaType.APPLICATION_JSON_TYPE).get(String.class));
		ScopeConfiguration conf=target(ServiceConstants.SDI.INTERFACE).request(MediaType.APPLICATION_JSON_TYPE).get(ScopeConfiguration.class);
		
	}
//	
	@Test
	public void getGeoServer() throws MalformedURLException{
		assumeTrue(isTestInfrastructureEnabled());
		
		ScopeConfiguration conf=target(ServiceConstants.SDI.INTERFACE).request(MediaType.APPLICATION_JSON_TYPE).get(ScopeConfiguration.class);
		
		assumeTrue(!conf.getByEngine(Engine.GS_ENGINE).isEmpty());	
		
		String hostname=new URL(conf.getByEngine(Engine.GS_ENGINE).get(0).getBaseEndpoint()).getHost();
		
		System.out.println(target(ServiceConstants.GeoServer.INTERFACE).
				path("configuration/"+hostname).request(MediaType.APPLICATION_JSON_TYPE).get(String.class));
	}
	
	
	
	@Test
	public void testHealthReport() {
		assumeTrue(isTestInfrastructureEnabled());
		System.out.println(target(ServiceConstants.SDI.INTERFACE).path("status").request(MediaType.APPLICATION_JSON_TYPE).get(String.class));
		System.out.println(target(ServiceConstants.SDI.INTERFACE).path("status").request(MediaType.APPLICATION_XML_TYPE).get(String.class));
	}
	
	
	// *********** METADATA
	
	@Test
	public void testGetTemplateList(){
		assumeTrue(isTestInfrastructureEnabled());
		List<TemplateDescriptor> result=target(ServiceConstants.Metadata.INTERFACE).
				path(ServiceConstants.Metadata.LIST_METHOD).
				request(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON).
				get(new GenericType<List<TemplateDescriptor>>() {});
		System.out.println(result);
	}
	
	

}
