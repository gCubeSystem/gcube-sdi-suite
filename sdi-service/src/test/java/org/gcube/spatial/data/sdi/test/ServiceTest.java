package org.gcube.spatial.data.sdi.test;

import static org.junit.Assume.assumeTrue;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.gcube.data.transfer.library.client.AuthorizationFilter;
import org.gcube.sdi.test.GCubeSDITest;
import org.gcube.spatial.data.sdi.SDIService;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.BeforeClass;

public class ServiceTest extends JerseyTest {

	protected static void checkResponse(Response toCheck) throws Exception{
		switch(toCheck.getStatusInfo().getFamily()){		
		case SUCCESSFUL : break;
		default : throw new Exception("Unexpected Response code : "+toCheck.getStatus(),new Exception(toCheck.readEntity(String.class)));
		}
	}
	
		
	@Override
	protected Application configure() {
		return new SDIService().register(RequestFilter.class);
	}
	
	
	@Override
	protected void configureClient(ClientConfig config) {
		// TODO Auto-generated method stub
		super.configureClient(config);
		
		config.register(MultiPartFeature.class);
		config.register(AuthorizationFilter.class);
	}

	public static boolean isTestInfrastructureEnabled() {
		return GCubeSDITest.isTestInfrastructureEnabled();
	}
	
	@BeforeClass
	public static void setContext() {
		assumeTrue(isTestInfrastructureEnabled());
		GCubeSDITest.setTestContext();
	}
}
