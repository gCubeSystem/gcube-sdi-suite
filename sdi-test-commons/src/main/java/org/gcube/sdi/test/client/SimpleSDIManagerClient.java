package org.gcube.sdi.test.client;

import java.util.List;

import javax.xml.transform.dom.DOMResult;
import javax.xml.ws.EndpointReference;

import org.gcube.common.clients.Call;
import org.gcube.common.clients.Plugin;
import org.gcube.common.clients.ProxyBuilder;
import org.gcube.common.clients.ProxyBuilderImpl;
import org.gcube.common.clients.config.ProxyConfig;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.common.gxrest.request.GXWebTargetAdapterRequest;
import org.gcube.common.gxrest.response.inbound.GXInboundResponse;
import org.gcube.spatia.data.model.profiles.ApplicationProfile;
import org.gcube.spatial.data.clients.SDIClientManager;
import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.sdi.interfaces.SDIManagement;
import org.gcube.spatial.data.sdi.model.ScopeConfiguration;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.gcube.spatial.data.sdi.model.health.HealthReport;
import org.gcube.spatial.data.sdi.model.service.GeoServiceDescriptor;
import org.w3c.dom.Node;

public class SimpleSDIManagerClient implements SDIManagement{

	public static ProxyBuilder<SDIManagement> contextService() {
		return new ProxyBuilderImpl<GXWebTargetAdapterRequest,SDIManagement>(
				new Plugin<GXWebTargetAdapterRequest, SDIManagement>(){
					@Override
					public String serviceClass() {return ServiceConstants.SERVICE_CLASS;}
					@Override
					public String serviceName() {return ServiceConstants.SERVICE_NAME;}
					@Override
					public String name() {return "sdi-service/gcube/service";}
					@Override
					public Exception convert(Exception fault, ProxyConfig<?, ?> config) {return fault;}
					@Override
					public String namespace() {return ServiceConstants.NAMESPACE;}
			
					
					@Override
					public GXWebTargetAdapterRequest resolve(EndpointReference epr, ProxyConfig<?, ?> config)
							throws Exception {
						DOMResult result = new DOMResult();
						epr.writeTo(result);
						Node node =result.getNode();
						Node child=node.getFirstChild();
						String address = child.getTextContent();
						GXWebTargetAdapterRequest request = 
								GXWebTargetAdapterRequest.newRequest(address).path(ServiceConstants.SDI.INTERFACE);
				                //set additional path parts or parameters here
				                return request;
				 
					}
					
					@Override
					public SDIManagement newProxy(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
						return new SimpleSDIManagerClient(delegate);}
				});
	}
	
	private final ProxyDelegate<GXWebTargetAdapterRequest> delegate;
	private SDIClientManager clientManager=new SDIClientManager();
	
	public SimpleSDIManagerClient(ProxyDelegate<GXWebTargetAdapterRequest> delegate) {
		this.delegate=delegate;
	}
	
	@Override
	public ScopeConfiguration getConfiguration() throws RemoteException {
		Call<GXWebTargetAdapterRequest, ScopeConfiguration> call=new Call<GXWebTargetAdapterRequest,ScopeConfiguration>(){
			@Override
			public ScopeConfiguration call(GXWebTargetAdapterRequest request) throws Exception {
				GXInboundResponse response = request.get();
				if (response.hasGXError()||
						response.getHTTPCode()<200||response.getHTTPCode()>299) {
                   RemoteException e=new RemoteException("Received error response.");
                   e.setContent(response.getStreamedContentAsString());
                   e.setRemoteMessage(response.getMessage());
                   e.setResponseHTTPCode(response.getHTTPCode());
                   throw e;
                }
                //MultiLocatorResponse is the expected content of the response in the form of a serialized Json
                return response.tryConvertStreamedContentFromJson(ScopeConfiguration.class);
			}
		};
		try {
			return delegate.make(call);			
		}catch(Exception e) {
			throw new RemoteException(e);
		}
	}

	@Override
	public HealthReport getReport() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApplicationProfile getProfile(String serviceClass, String serviceName) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SDIGenericPlugin getClientByEngineId(String engineId) throws Exception {		
		List<? extends GeoServiceDescriptor> desc=getConfiguration().getByEngine(engineId);
		
		if(desc==null|| desc.isEmpty())	throw new Exception("No "+engineId+" available in current context. Check ScopeConfiguration object.");
		
		for(GeoServiceDescriptor d:desc) {
			SDIGenericPlugin c=clientManager.get(d);
			if(c!=null) return c;
		}
		
		throw new RuntimeException("No implementation available for "+engineId);
	}
	
//	@Override
//	public ConnectionDescriptor getService() throws RemoteException {
//		delegate.make(new Call<GXWebTargetAdapterRequest, ConnectionDescriptor>() {
//			@Override
//			public ConnectionDescriptor call(GXWebTargetAdapterRequest endpoint) throws Exception {
//				return new ConnectionDescriptor(endpoint.)
//			}
//		});
//		
//		return new ConnectionDescriptor(endpoint)
//	}
}
