package org.gcube.sdi.test;

import static org.junit.Assume.assumeTrue;

import org.gcube.sdi.test.client.SimpleSDIManagerClient;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.sdi.interfaces.SDIManagement;
import org.gcube.spatial.data.sdi.model.ScopeConfiguration;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.junit.BeforeClass;


public class GCubeSDITest {
	
	public static String getContext() {
		String testContext=System.getProperty("testContext");
		if(testContext==null) {
			// trying with env
			testContext=System.getenv("testContext");
		}
			
		System.out.println("TEST CONTEXT = "+testContext);
		return testContext;
	}

	

	public static boolean isTestInfrastructureEnabled() {
		return getContext()!=null;
	}

	@BeforeClass
	public static void setTestContext() {
		assumeTrue(isTestInfrastructureEnabled());	
		TokenSetter.set(getContext());		
	}
	
	
	public static ScopeConfiguration scopeConfiguration() throws RemoteException {		
		return SimpleSDIManagerClient.contextService().build().getConfiguration();
	}
	
	public static SDIManagement manager() throws RemoteException {		
		return SimpleSDIManagerClient.contextService().build();
	}
	
}
