This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.gcube-geoserver-connector

## [v0.2.0-SNAPSHOT] - 2021-2-11
Migrated to git
Embedded gcube-application-handlers.xml file
