This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.spatial.data.sdi-generic-client

## [v0.0.1-SNAPSHOT] - 2021-2-11
First release
