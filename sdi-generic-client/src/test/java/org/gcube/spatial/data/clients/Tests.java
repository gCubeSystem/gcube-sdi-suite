package org.gcube.spatial.data.clients;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;
import static org.junit.Assume.assumeTrue;

import java.net.URI;
import java.util.List;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.sdi.test.GCubeSDITest;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.sdi.model.ServiceConstants;
import org.gcube.spatial.data.sdi.model.credentials.AccessType;
import org.gcube.spatial.data.sdi.model.service.GeoServiceDescriptor;
import org.json.simple.JSONObject;
import org.junit.Test;

public class Tests extends GCubeSDITest{



	@Test
	public void testContextAuth() throws Exception {
		assumeTrue(isTestInfrastructureEnabled());
		
		
		
//		String url=scopeConfiguration().getByEngine(Engine.GS_ENGINE).get(0).getBaseEndpoint();
		
		
		
		URI uri=queryForGCoreEndpoint(ServiceConstants.SERVICE_CLASS, ServiceConstants.SERVICE_NAME).get(0).profile().endpointMap().
		get("org.gcube.spatial.data.sdi.SDIService").uri();
		
		System.out.println(uri+"\t"+new TestClient(new ConnectionDescriptor(uri.toString())).get("SDI"));
	}
	
	
	@Test 
	public void testRedirection() throws Exception {
		assumeTrue(isTestInfrastructureEnabled());
		
		assumeTrue(isTestInfrastructureEnabled());
		
		
		
		GeoServiceDescriptor service=scopeConfiguration().getByEngine(Engine.GS_ENGINE).get(0);
		
		service.setBaseEndpoint(service.getBaseEndpoint().replaceAll("https", "http"));
		
		
		TestClient client=new TestClient(service.getConnection());
		System.out.println(service.getBaseEndpoint()+"\t"+client.get(""));
	}
	

	
	
	@Test
	public void testBasicAuth() throws Exception {
		assumeTrue(isTestInfrastructureEnabled());
		
		
		
		GeoServiceDescriptor service=scopeConfiguration().getByEngine(Engine.GS_ENGINE).get(0);
		
		
		TestClient client=new TestClient(service.getConnection());
		client.setBasePath("rest");
		client.authenticate(AccessType.ADMIN);
		System.out.println(service.getBaseEndpoint()+"\t"+client.get("workspaces"));
	}
	
	
	@Test
	public void testSerialization() throws Exception {
		assumeTrue(isTestInfrastructureEnabled());
		
		
		
		GeoServiceDescriptor service=scopeConfiguration().getByEngine(Engine.GS_ENGINE).get(0);
		
		
		TestClient client=new TestClient(service.getConnection());
		client.setBasePath("rest");
		client.authenticate(AccessType.ADMIN);
		System.out.println(service.getBaseEndpoint()+"\t"+client.get("workspaces",JSONObject.class).toString());
	}
	
	
	private static List<GCoreEndpoint> queryForGCoreEndpoint(String serviceClass,String serviceName){
		
		
		SimpleQuery query =queryFor(GCoreEndpoint.class);
		query.addCondition("$resource/Profile/ServiceClass/text() eq '"+serviceClass+"'")
		.addCondition("$resource/Profile/ServiceName/text() eq '"+serviceName+"'");				
		//		.setResult("$resource/Profile/AccessPoint");

		DiscoveryClient<GCoreEndpoint> client = clientFor(GCoreEndpoint.class);

		return client.submit(query);
	}
	

	
	
}
