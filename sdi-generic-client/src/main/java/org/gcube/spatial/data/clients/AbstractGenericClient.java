package org.gcube.spatial.data.clients;

import java.lang.reflect.InvocationTargetException;

import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;

public abstract class AbstractGenericClient implements SDIGenericPlugin {

	private ConnectionDescriptor conn;
	private Class<?> restClientClass;
	
	
	public AbstractGenericClient(Class<?> clazz) {
		this.restClientClass=clazz;
	}

	@Override
	public Object getRESTClient() {
		try {
			return restClientClass.getConstructor(ConnectionDescriptor.class).newInstance(conn);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("Unable to instantiate class "+restClientClass,e);
		}
	}

	@Override
	public SDIGenericPlugin at(ConnectionDescriptor conn) {
		this.conn=conn;
		return this;
	}

}
