package org.gcube.spatial.data.sdi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.gcube.spatial.data.clients.SDIClientManager;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.clients.model.engine.ComparableVersion;
import org.gcube.spatial.data.clients.model.engine.Range;
import org.junit.Test;

public class Clients {

	@Test
	public void testRanges() {
		
		
		
		assertFalse(new Range("1.1.0",null).supports("0.0.1"));
		assertFalse(new Range(null,"3.4").supports("4.2.1"));
		
		
		assertTrue(new Range("1.1.0",null).supports("2.0.1"));
		assertTrue(new Range("1.1.0","2.2").supports("2.0.1"));
		assertTrue(new Range("1.2",null).supports("1.2.1"));
		assertTrue(new Range(null,"3.4").supports("1.2.1"));
	}


	
	@Test
	public void testLoading() {
		System.out.println(new SDIClientManager().get("dumb-gn", "1.5.7rc").getInfo());
	}
}
