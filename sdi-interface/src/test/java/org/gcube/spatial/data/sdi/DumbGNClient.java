package org.gcube.spatial.data.sdi;

import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.clients.model.engine.Range;

public class DumbGNClient implements SDIGenericPlugin{

	
	ClientInfo gnClient=new ClientInfo("dumb-gn","GeoNetwork 2","Client to communicate with GN 2","org.gcube.sdi.some.clients.Client",
			new Engine("dumb-gn","",new Range("1.1.0","2.20")));
	
	@Override
	public ClientInfo getInfo() {
		return gnClient;
	}
	
	@Override
	public Object getRESTClient() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public SDIGenericPlugin at(ConnectionDescriptor conn) {
		// TODO Auto-generated method stub
		return null;
	}
}
