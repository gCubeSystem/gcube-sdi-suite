package org.gcube.spatial.data.clients.model.engine;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Range{
	
	
	private ComparableVersion upperBound=null;
	private ComparableVersion lowerBound=null;
	
	public void setLowerBound(ComparableVersion lowerBound) {
		if(upperBound!=null&&lowerBound.compareTo(upperBound)>=0) 
			throw new IllegalArgumentException("Lower Bound ("+lowerBound.getCanonical()+") must be below Upper Bound ("+upperBound.getCanonical()+")");  
		this.lowerBound = lowerBound;
	}

	public void setUpperBound(ComparableVersion upperBound) {
		if(lowerBound!=null&&upperBound.compareTo(lowerBound)<=0) 
			throw new IllegalArgumentException("Upper Bound ("+upperBound.getCanonical()+") must be above Lower Bound ("+lowerBound.getCanonical()+")");  
		this.upperBound = upperBound;
	}
	
	
	public Range(String lower,String upper) {
		if(lower!=null) setLowerBound(new ComparableVersion(lower));
		if(upper!=null) setUpperBound(new ComparableVersion(upper));
	}
	
	
	public Range(ComparableVersion lower,ComparableVersion upper) {
		this.setLowerBound(lower);
		this.setUpperBound(upper);
	}
	
	public boolean supports(String target) {
		return supports(new ComparableVersion(target));
	}
	
	public boolean supports(ComparableVersion target) {
		if(target==null) throw new IllegalArgumentException("Target version cannot be null.");
		if(upperBound!=null&&target.compareTo(upperBound)>0) 
			return false; // target over Upper Bound
		
		if(lowerBound!=null&&target.compareTo(lowerBound)<0) 
			return false; // target under Lower Bound
		
		return true;
	}
	
	@Override
	public String toString() {
		return "["+(lowerBound!=null?lowerBound.getCanonical():"...")+","+
				(upperBound!=null?upperBound.getCanonical():"...")+"]";
	}
}