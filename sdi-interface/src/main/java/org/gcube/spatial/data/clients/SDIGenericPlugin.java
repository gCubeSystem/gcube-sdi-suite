package org.gcube.spatial.data.clients;

import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;

public interface SDIGenericPlugin {

	
	public ClientInfo getInfo();
	
	public Object getRESTClient();
	
	public SDIGenericPlugin at(ConnectionDescriptor conn);
	
}
