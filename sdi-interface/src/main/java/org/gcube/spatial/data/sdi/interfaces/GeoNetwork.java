package org.gcube.spatial.data.sdi.interfaces;

import org.gcube.spatial.data.sdi.model.faults.ConfigurationException;
import org.gcube.spatial.data.sdi.model.faults.InternalException;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.gcube.spatial.data.sdi.model.gn.LoginLevel;
import org.gcube.spatial.data.sdi.model.gn.query.GNSearchRequest;
import org.gcube.spatial.data.sdi.model.gn.query.GNSearchResponse;
import org.gcube.spatial.data.sdi.model.metadata.MetadataInfo;



public interface GeoNetwork {

	// MANAGEMENT
	
//	public Credentials getCredentials(String host);
	
	
	
	public GNSearchResponse query(GNSearchRequest request)
			throws InternalException,RemoteException,ConfigurationException;

	
	public Metadata getById(long id) throws InternalException,RemoteException,ConfigurationException;

	public Metadata getById(String UUID) throws InternalException,RemoteException,ConfigurationException;

	public String getByIdAsRawString(String UUID) throws InternalException,RemoteException,ConfigurationException;
	
	
	public void login()throws InternalException,RemoteException,ConfigurationException;
	
	public void login(LoginLevel level)throws InternalException,RemoteException,ConfigurationException;
	
	public void logout() throws InternalException,RemoteException,ConfigurationException;
	
	public MetadataInfo getInfo(Long id) throws InternalException,RemoteException,ConfigurationException;

    public MetadataInfo getInfo(String uuid) throws InternalException,RemoteException,ConfigurationException;
	
	
}
