package org.gcube.spatial.data.clients.model;

import org.gcube.spatial.data.clients.model.engine.Engine;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientInfo {

	
	private String id;
	private String name;
	private String description;
	private String clientImplementationClass;

	
	private Engine supportedEngine;
}
