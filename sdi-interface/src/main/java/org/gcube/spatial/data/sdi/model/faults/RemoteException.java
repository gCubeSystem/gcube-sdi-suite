package org.gcube.spatial.data.sdi.model.faults;

import lombok.Data;

@Data
public class RemoteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3758504937629040958L;

	public RemoteException() {
		// TODO Auto-generated constructor stub
	}

	public RemoteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RemoteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public RemoteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RemoteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	private String remoteMessage=null;
	
	private Integer responseHTTPCode=0;
	
	private String content=null;
	
	
	
}
