package org.gcube.spatial.data.sdi.model.service;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.sdi.model.credentials.Credentials;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GeoServerDescriptor extends GeoServiceDescriptor {

		
	
	public GeoServerDescriptor(Version version, String baseEndpoint, List<Credentials> accessibleCredentials,
			String confidentialWorkspace, String contextVisibilityWorkspace, String sharedWorkspace,
			String publicWorkspace) {
		super(version, baseEndpoint, accessibleCredentials);
		this.confidentialWorkspace = confidentialWorkspace;
		this.contextVisibilityWorkspace = contextVisibilityWorkspace;
		this.sharedWorkspace = sharedWorkspace;
		this.publicWorkspace = publicWorkspace;
	}
	
	private String confidentialWorkspace;
	
	private String contextVisibilityWorkspace;
	
	private String sharedWorkspace;
	
	private String publicWorkspace;
	
	private Long hostedLayersCount;
	
	private String engineId=Engine.GS_ENGINE;
	
	
}
