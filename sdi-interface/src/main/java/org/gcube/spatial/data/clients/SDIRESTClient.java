package org.gcube.spatial.data.clients;

import org.gcube.spatial.data.clients.model.ConnectionDescriptor;

public interface SDIRESTClient {


	// Management
	public boolean exist();

	public ConnectionDescriptor getInstance();

	
	
}
