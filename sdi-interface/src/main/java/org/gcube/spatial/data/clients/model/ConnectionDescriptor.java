package org.gcube.spatial.data.clients.model;

import java.util.ArrayList;
import java.util.List;

import org.gcube.spatial.data.sdi.model.credentials.Credentials;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Delegate;

@Data
public class ConnectionDescriptor {

	@NonNull
	private String endpoint;
	
	@Delegate
	private List<Credentials> credentials=new ArrayList<Credentials>();
	
	private String engineUniqueID;
	private String version;
}
