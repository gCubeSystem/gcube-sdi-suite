package org.gcube.spatial.data.clients;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ServiceLoader;

import org.gcube.spatial.data.clients.model.ClientInfo;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.sdi.model.service.GeoServiceDescriptor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SDIClientManager {

	private List<SDIGenericPlugin> clients=new ArrayList<SDIGenericPlugin>();
	
	public SDIClientManager() {		
		ServiceLoader.load(SDIGenericPlugin.class).forEach((SDIGenericPlugin c)->{clients.add(c);});
		log.info("Loaded {} clients ",clients.size());
		clients.forEach((SDIGenericPlugin c)->{
			log.debug("Loaded "+c.getInfo());
		});
	}
	
	
	/**
	 * Returns the implementation for the target engine. Null if no suitable plugin is found
	 * 
	 * @param engine
	 * @param version
	 * @return
	 */
	public SDIGenericPlugin get(String engine,String version) {
		log.info("Looking for clients [target : {} {} ] ",engine,version);
		for(SDIGenericPlugin c:clients) {
			if(c.getInfo().getSupportedEngine().getEngineUniqueString().equals(engine)
				&& c.getInfo().getSupportedEngine().getRange().supports(version))
				return c;
		}
		return null;
	}
	
	
	/**
	 * Returns the implementation for the target engine's Connection. Null if no suitable plugin is found
	 * 
	 * @param engine
	 * @param version
	 * @return
	 * @throws Exception 
	 */
	public SDIGenericPlugin get(ConnectionDescriptor conn) throws Exception {
		SDIGenericPlugin toReturn=get(conn.getEngineUniqueID(),conn.getVersion());
		if(toReturn==null) throw new Exception("Unable to get client for "+conn.getEngineUniqueID()+" "+conn.getVersion());
		return toReturn.at(conn);
	}
	

	/**
	 * Returns the implementation for the target service. Null if no suitable plugin is found
	 * 
	 * @param engine
	 * @param version
	 * @return
	 * @throws Exception 
	 */
	public SDIGenericPlugin get(GeoServiceDescriptor service) throws Exception {
		return get(service.getConnection());
	}
	
	
	public List<ClientInfo> list(){
		List<ClientInfo> toReturn=new ArrayList<ClientInfo>();
		clients.forEach((SDIGenericPlugin c)->{toReturn.add(c.getInfo());});
		return toReturn;
	}
	
	public List<ClientInfo> list(String engineId){
		List<ClientInfo> toReturn=new ArrayList<ClientInfo>();
		clients.forEach((SDIGenericPlugin c)->{
			if(c.getInfo().getSupportedEngine().getEngineUniqueString().equals(engineId))
			toReturn.add(c.getInfo());});
		return toReturn;
	}
}
