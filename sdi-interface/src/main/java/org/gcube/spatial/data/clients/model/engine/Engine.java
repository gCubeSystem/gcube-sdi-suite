package org.gcube.spatial.data.clients.model.engine;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Engine {

	public static final String GN_ENGINE="geonetwork";
	public static final String GS_ENGINE="geoserver";
	public static final String TH_ENGINE="thredds";
	
	
	private String engineUniqueString;
	private String description;
	private Range range;
	
}
