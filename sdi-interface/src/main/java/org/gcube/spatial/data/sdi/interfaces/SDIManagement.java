package org.gcube.spatial.data.sdi.interfaces;

import org.gcube.spatia.data.model.profiles.ApplicationProfile;
import org.gcube.spatial.data.clients.SDIGenericPlugin;
import org.gcube.spatial.data.clients.model.ConnectionDescriptor;
import org.gcube.spatial.data.sdi.model.ScopeConfiguration;
import org.gcube.spatial.data.sdi.model.faults.RemoteException;
import org.gcube.spatial.data.sdi.model.health.HealthReport;

public interface SDIManagement {

	public ScopeConfiguration getConfiguration() throws RemoteException;
	public HealthReport getReport() throws RemoteException;
	public ApplicationProfile getProfile(String serviceClass,String serviceName) throws RemoteException;
	
	
	
	public SDIGenericPlugin getClientByEngineId(String engineId) throws Exception;

}
