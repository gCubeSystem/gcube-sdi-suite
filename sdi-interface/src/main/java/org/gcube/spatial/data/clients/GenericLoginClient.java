package org.gcube.spatial.data.clients;

import org.gcube.spatial.data.sdi.model.credentials.AccessType;

public interface GenericLoginClient extends SDIRESTClient{

	
	public void authenticate();
	
	public void authenticate(AccessType type);
	
	 
}
