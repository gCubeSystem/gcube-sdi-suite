package org.gcube.spatial.data.sdi.model;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.spatial.data.clients.model.engine.Engine;
import org.gcube.spatial.data.clients.model.engine.Range;
import org.gcube.spatial.data.sdi.model.service.GeoNetworkDescriptor;
import org.gcube.spatial.data.sdi.model.service.GeoServerDescriptor;
import org.gcube.spatial.data.sdi.model.service.GeoServiceDescriptor;
import org.gcube.spatial.data.sdi.model.service.ThreddsDescriptor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Slf4j
public class ScopeConfiguration {
	
	
	
	@NonNull
	private String contextName;
	
	@NonNull
	private List<GeoNetworkDescriptor> geonetworkConfiguration;
	@NonNull
	private List<GeoServerDescriptor> geoserverClusterConfiguration;
	@NonNull
	private List<ThreddsDescriptor> threddsConfiguration;	
	
	
	
	public List<? extends GeoServiceDescriptor> getByEngine(String engineId) {
		List<? extends GeoServiceDescriptor> descList=Collections.emptyList();
		
		switch (engineId) {
		case Engine.GN_ENGINE:
			if(getGeonetworkConfiguration().size()>0)				
				descList=getGeonetworkConfiguration();
			break;
		case Engine.GS_ENGINE :
			if(getGeoserverClusterConfiguration().size()>0)
				descList=getGeoserverClusterConfiguration();
			break;
		case Engine.TH_ENGINE : 
			if(getThreddsConfiguration().size()>0)
				descList=getThreddsConfiguration();
			break;
		default:			
			break;
		}
		return descList;
	}
	
	public GeoServiceDescriptor getByRange(List<? extends GeoServiceDescriptor> descList ,Range range) {
		if(descList==null||descList.isEmpty()) return null;
		// Get first by range
		for(GeoServiceDescriptor d:descList) {
			if(range.supports(d.getVersion().asComparable())) {
				return d;
			}
		}
		// not in range
		return null;
	}
	
	public GeoServiceDescriptor getByEngineAndVersion(String engineId,Range range) {
		return getByRange(getByEngine(engineId), range);
	}
	
	
  
}
